ARG SOURCE_DOCKER_REGISTRY=localhost:5000

FROM ${SOURCE_DOCKER_REGISTRY}/ubuntu_opt_autodockvina:1.1.2 as opt_autodockvina

RUN apt update -y && \
    apt install -y python3 && \
    apt autoremove -y && \
    apt clean

COPY bin/ /opt/bin/

ENV PATH /opt/bin/:${PATH}

